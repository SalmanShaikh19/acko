import React from "react";
import "antd/dist/antd.css";
import "../css/App.css";
import { Col, Row, Card } from "antd";

function App() {
  return (
    <>
    <div style={{ backgroundColor: "hsl(0, 0%, 93%)" , minHeight:'100vh'}}>
     <Card  style={{ backgroundColor: "hsl(0, 0%, 93%)" }}>
      <Row justify="center">
        <div className='heading' >Welcome to acko</div>
      </Row>
     
        <Row className='line'>
          <Col span={12} >
            
              <div className="upperName">Insurance Provider</div>
              <span className="upperName" style={{color:'blue'}}>Acko Insurance Co. Ltd.</span>
            
          </Col>
          <Col span={9} offset={3} >
         
              <div className="upperName">Vehicle Type</div>
              <span className="upperName" style={{color:'blue'}}>Goods Carrier</span>
      
          </Col>
        </Row>

       <Row className='line'>
          <Col span={12} >
            
              <div className="upperName">Vehicle Chassis No.</div>
              <span className="upperName" style={{color:'blue'}}>76300</span>
            
          </Col>
          <Col span={9} offset={3} >
         
              <div className="upperName">Policy Number</div>
              <span className="upperName" style={{color:'blue'}}>DBTR00624116116100</span>
      
          </Col>
        </Row>

       <Row className='line'>
          <Col span={12} >
            
              <div className="upperName">Vehicle Make</div>
              <span className="upperName" style={{color:'blue'}}>Tata</span>
            
          </Col>
          <Col span={9} offset={3} >
         
              <div className="upperName">Valid Upto</div>
              <span className="upperName" style={{color:'blue'}}>28 August 2023</span>
      
          </Col>
        </Row>
       <Row className='line'>
          <Col span={12} >
            
              <div className="upperName">Vehicle Model</div>
              <span className="upperName" style={{color:'blue'}}>Tata Ace</span>
            
          </Col>
          <Col span={9} offset={3} >
         
              <div className="upperName">Policy Status</div>
              <span className="upperName" style={{color:'blue'}}>Issued</span>
      
          </Col>
        </Row>

       <Row className='line'>
          <Col span={12} >
            
              <div className="upperName">Customer Name</div>
              <span className="upperName" style={{color:'blue'}}>Nagesh Ramchandra Asabe</span>
            
          </Col>
          <Col span={9} offset={3} >
         
              <div className="upperName">Vehicle Reg. No.</div>
              <span className="upperName" style={{color:'blue'}}>MH13AN8407</span>
      
          </Col>
        </Row>

       <Row className='line'>
          <Col span={12} >
            
              <div className="upperName">Vehicle Engine No.</div>
              <span className="upperName" style={{color:'blue'}}>L3462</span>
            
          </Col>
          <Col span={9} offset={3} >
         
              <div className="upperName">Total Liability</div>
              <span className="upperName" style={{color:'blue'}}>14563</span>
      
          </Col>
        </Row>

       <Row className='line'>
          <Col span={12} >
            
              <div className="upperName">Net Premium</div>
              <span className="upperName" style={{color:'blue'}}>14563</span>
            
          </Col>
          <Col span={9} offset={3} >
         
              <div className="upperName">Final Premium</div>
              <span className="upperName" style={{color:'blue'}}>17859</span>
      
          </Col>
        </Row>
      </Card>
      </div>
    </>
  );
}

export default App;
